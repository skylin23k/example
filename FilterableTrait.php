<?php


namespace App\Util\Model;

use Doctrine\ORM\QueryBuilder;

trait FilterableTrait
{
    protected function processFilter(QueryBuilder $qb, $filters)
    {
        if (is_array($filters)) {
            $filtersClauses = $qb->expr()->andX();

            foreach ($filters as $filterName => $filterValue) {
                $filterMethod = $filterName . 'Filter';
                if (method_exists($this, $filterMethod)) {
                    $this->$filterMethod($qb, $filtersClauses, $filterValue);
                }
            }

            if ($filtersClauses->count()) {
                $where = $qb->getDQLPart('where');
                if ($where) {
                    $qb->andWhere($filtersClauses);
                } else {
                    $qb->where($filtersClauses);
                }
            }
        }

        return $qb;
    }
}