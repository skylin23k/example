<?php

namespace App\Controller\Api;

use App\Entity\Hit;
use App\Presenter\NpaPresenter;
use App\Service\HitService;
use App\Service\NpaService;
use App\Service\ReferenceService;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;

/**
 * @Route("/api/npa")
 */
class NpaController extends AbstractController
{
    /**
     * @Route("/by-city", name="api_npa_by-city")
     * @param Request $request
     * @param ReferenceService $referenceService
     * @param NpaService $npaService
     * @param HitService $hitService
     * @return JsonResponse
     */
    public function byCity(
        Request $request,
        ReferenceService $referenceService,
        NpaService $npaService,
        HitService $hitService
    ) {
        $npa = null;

        $cityName = $request->get('city');
        $city = $referenceService->getCityByName($cityName);

        $userId = $request->get('uniqid');


        if ($city) {
            $npa = $npaService->getLessUsesNpaByCity($city);
        }

        $npaPresenter = NpaPresenter::create($npa);

        $hitService->registerHit(
            Hit::NPA_BY_CITY,
            [
                'city' => $cityName,
                'npa' => $npaPresenter,
                'referal' => $npa ? $npa->getReferal() : null,
                'userId' => $userId
            ]
        );

        return $this->json($npaPresenter);
    }

//    /**
//     * @Route("/by-coord", name="api_npa_by-coord")
//     * @param Request $request
//     * @param ReferenceService $referenceService
//     * @param NpaService $npaService
//     * @param HitService $hitService
//     * @return JsonResponse
//     */
//    public function byCoord(
//        Request $request,
//        ReferenceService $referenceService,
//        NpaService $npaService,
//        HitService $hitService
//    ) {
//        $npa = null;
//
//        $longitude = floatval($request->get('longitude'));
//        $latitude = floatval($request->get('latitude'));
//
//        $city = $referenceService->getNearestCity(
//            $longitude,
//            $latitude
//        );
//
//
//        if ($city) {
//            $npa = $npaService->getLessUsesNpaByCity($city);
//        }
//
//        $npaPresenter = NpaPresenter::create($npa);
//
//        $hitService->registerHit(
//            Hit::NPA_BY_COORD,
//            [
//                'coord' => [
//                    'longitude' => $longitude,
//                    'latitude' => $latitude
//                ],
//                'city' => $city ? $city->getName() : null,
//                'npa' => $npaPresenter
//            ]
//        );
//
//        return $this->json($npaPresenter);
//    }

    /**
     * @Route("/by-referal", name="api_npa_by-referal")
     * @param Request $request
     * @param NpaService $npaService
     * @param HitService $hitService
     * @return JsonResponse
     */
    public function byReferal(Request $request, NpaService $npaService, HitService $hitService)
    {
        $npa = null;
        $referalCode = $request->get('referal', null);
        $userId = $request->get('uniqid');

        if (isset($referalCode)) {
            $npa = $npaService->getByReferalCode($referalCode);
        }

        $npaPresenter = NpaPresenter::create($npa);

        if ($npa) {
            $cityName = $npa->getCity()->getName();

            $hitService->registerHit(
                Hit::NPA_BY_REFERAL,
                [
                    'city' => $cityName,
                    'npa' => $npaPresenter,
                    'referal' => $npa ? $npa->getReferal() : null,
                    'userId' => $userId
                ]
            );
        }

        return $this->json($npaPresenter);
    }


    /**
     * @Route("/used")
     * @param Request $request
     * @param NpaService $npaService
     * @return JsonResponse
     */
    public function npaUsed(Request $request, NpaService $npaService)
    {
        $npaReferal = $request->get('referal');
        $npa = $npaService->getByReferalCode($npaReferal);

        if ($npa) {
            $npa->setUsesCount($npa->getUsesCount() + 1);
            $npaService->save($npa);
            return $this->json([]);
        }

        return $this->json([], 500);
    }
}

