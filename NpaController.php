<?php

namespace App\Controller\Administration;

use App\Entity\Npa;
use App\Form\Administration\NpaEditType;
use App\Presenter\Administration\Npa\ListedNpaPresenter;
use App\Presenter\PaginatorPresenter;
use App\Service\NpaService;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\File\UploadedFile;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

/**
 * @Route("/administration/npa")
 */
class NpaController extends AbstractController
{

    /**
     * @Route(path="/", name="administration_npa_index")
     * @return Response
     */
    public function index()
    {
        return $this->render('administration/npa/index.html.twig');
    }

    /**
     * @Route("/add", name="administration_npa_add")
     * @Route("/edit/{item}", name="administration_npa_edit")
     * @param Request $request
     * @param NpaService $service
     * @param Npa|null $item
     * @return Response
     */
    public function editAction(
        Request $request,
        NpaService $service,
        Npa $item = null
    ) {
        if ($item === null) {
            $item = new Npa();
        }
        $form = $this->createForm(NpaEditType::class, $item);

        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $service->save($item);

            return $this->redirectToRoute(
                'administration_npa_index'
            );
        }

        return $this->render(
            "administration/npa/edit.html.twig",
            [
                'form' => $form->createView()
            ]
        );
    }

    /********************************************************************************/
    /*                                 REST SERVICES                                */
    /********************************************************************************/

    /**
     * @Route("/rest/get-items")
     * @param Request $request
     * @param NpaService $service
     * @return Response
     */
    public function getItemsAction(Request $request, NpaService $service)
    {
        $page = $request->get('page', 1);
        $filter = $request->get('filter', []);
        $sort = $request->get('sort', []);

        $paginator = $service->getPaginator($sort, $filter);

        return new JsonResponse(
            new PaginatorPresenter(
                $paginator,
                $page,
                50,
                ListedNpaPresenter::class
            )
        );
    }

    /**
     * @Route("/rest/remove/{id}")
     * @param Npa $npa
     * @param NpaService $npaService
     * @return JsonResponse
     */
    public function remove(Npa $npa, NpaService $npaService)
    {
        if ($npa) {
            $npaService->remove($npa);
        }

        return $this->json([]);
    }


    /**
     * @Route("/import", name="administration_npa_import")
     * @param Request $request
     * @param NpaService $npaService
     * @return JsonResponse
     */
    public function import(Request $request, NpaService $npaService)
    {
        try {
            /** @var UploadedFile $uploadedFile */
            $uploadedFile = $request->files->get('filedata');

            if ($uploadedFile === null) {
                throw new \RuntimeException('Can\'t find uploaded file!');
            }

            $npaService->import($uploadedFile->getRealPath());
        } catch (\Exception $ex) {
            return new JsonResponse(
                [
                    'error' => $ex->getMessage(),
                    'line' => $ex->getLine(),
                    'file' => $ex->getFile(),
                ],
                500
            );
        }

        return new JsonResponse();
    }
}
