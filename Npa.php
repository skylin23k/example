<?php

namespace App\Entity;

use App\Entity\Reference\City;
use App\Repository\NpaRepository;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;
use Symfony\Bridge\Doctrine\Validator\Constraints\UniqueEntity;

/**
 * @ORM\Entity(repositoryClass=NpaRepository::class)
 * @ORM\HasLifecycleCallbacks
 * @UniqueEntity(fields={"referal"}, message="Это значение уже используется")
 */
class Npa
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=255)
     * @Assert\NotBlank
     */
    private $name;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     * @Assert\NotBlank
     */
    private $referal;

    /**
     * @var City
     * @ORM\ManyToOne(targetEntity=City::class, cascade={"persist"})
     * @ORM\JoinColumn(nullable=true, onDelete="SET NULL")
     */
    private $city;

    /**
     * @var int
     * @ORM\Column(type="integer", options={"default": 0})
     */
    private $usesCount;

    /**
     * @ORM\Column(type="datetime", nullable=true)
     */
    protected $createdAt;

    /**
     * @ORM\Column(type="datetime", nullable=true)
     */
    protected $updatedAt;

    public function __construct()
    {
        $this->usesCount = 0;
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getName(): ?string
    {
        return $this->name;
    }

    public function setName(string $name): self
    {
        $this->name = $name;

        return $this;
    }

    public function getReferal(): ?string
    {
        return $this->referal;
    }

    public function setReferal(?string $referal): self
    {
        $this->referal = $referal;

        return $this;
    }

    /**
     * @return City
     */
    public function getCity(): ?City
    {
        return $this->city;
    }

    /**
     * @param City $city
     * @return $this
     */
    public function setCity(City $city): self
    {
        $this->city = $city;

        return $this;
    }

    /**
     * @return int
     */
    public function getUsesCount(): ?int
    {
        return $this->usesCount;
    }

    /**
     * @param int $usesCount
     * @return Npa
     */
    public function setUsesCount(int $usesCount): self
    {
        $this->usesCount = $usesCount;

        return $this;
    }

    /**
     * @ORM\PrePersist
     */
    public function createTimestamps(): void
    {
        $this->createdAt = new \DateTime('now');
        $this->updatedAt = new \DateTime('now');
    }

    /**
     * @ORM\PreUpdate
     */
    public function updateTimestamps(): void
    {
        $this->updatedAt = new \DateTime('now');
    }
}
