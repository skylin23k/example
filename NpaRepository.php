<?php

namespace App\Repository;

use App\Entity\Npa;
use App\Util\Model\FilterableTrait;
use App\Util\Model\SortableTrait;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @method Npa|null find($id, $lockMode = null, $lockVersion = null)
 * @method Npa|null findOneBy(array $criteria, array $orderBy = null)
 * @method Npa[]    findAll()
 * @method Npa[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class NpaRepository extends ServiceEntityRepository
{
    use FilterableTrait;
    use SortableTrait;

    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, Npa::class);
    }

    public function getFilterQuery($sortFields, $filters)
    {
        $qb = $this->createQueryBuilder('npa');
        $qb = $this->processFilter($qb, $filters);
        $qb = $this->processSort($qb, $sortFields);

        return $qb->getQuery();
    }
}
