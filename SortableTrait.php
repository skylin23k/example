<?php


namespace App\Util\Model;

use Doctrine\ORM\QueryBuilder;

trait SortableTrait
{
    protected function processSort(QueryBuilder $qb, $sortFields)
    {
        if (is_array($sortFields)) {
            foreach ($sortFields as $field => $order) {
                $order = strtoupper($order);
                $order = ($order === 'ASC') ? 'ASC' : 'DESC';
                $qb->addOrderBy($field, $order);
            }
        }
        return $qb;
    }
}