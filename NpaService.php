<?php

namespace App\Service;

use App\Entity\Npa;
use App\Entity\Reference\City;
use App\Repository\NpaRepository;
use Box\Spout\Common\Entity\Row;
use Box\Spout\Reader\Common\Creator\ReaderEntityFactory;
use Doctrine\ORM\EntityManagerInterface;
use Doctrine\ORM\Tools\Pagination\Paginator;

class NpaService
{
    /** @var EntityManagerInterface */
    public $em;

    private $referenceService;

    public function __construct(
        EntityManagerInterface $em, ReferenceService $referenceService
    ) {
        $this->em = $em;
        $this->referenceService = $referenceService;
    }

    /**
     * @param Npa $item
     * @param bool $needFlush
     */
    public function save(Npa $item, $needFlush = true)
    {
        if (!$item->getId()) {
            $this->em->persist($item);
        }
        if ($needFlush) {
            $this->em->flush();
        }
    }

    /**
     * @param Npa $item
     * @param bool $needFlush
     */
    public function remove(Npa $item, $needFlush = true)
    {
        $this->em->remove($item);
        if ($needFlush) {
            $this->em->flush();
        }
    }

    /**
     * @param City|null $city
     * @return Npa|null
     */
    public function getRandomNpaByCity(?City $city)
    {
        if ($city) {
            $npas = $this->getRepository()->findBy(
                [
                    'city' => $city
                ]
            );

            if (count($npas) > 0) {
                return $npas[array_rand($npas)];
            }
        }

        return null;
    }

    public function getLessUsesNpaByCity(City $city): ?Npa
    {
        $npas = $this->getRepository()->findBy(['city' => $city], ['usesCount' => 'ASC'] );

        if (count($npas) > 0) {
            return $npas[0];
        }

        return null;
    }

    /**
     * @param string $code
     * @return Npa|null
     */
    public function getByReferalCode(?string $code): ?Npa
    {
        if ($code) {
            return $this->getRepository()->findOneBy(['referal' => $code]);
        }

        return null;
    }

    /**
     * @return array
     */
    public function getAllItems(): array
    {
        return $this->getRepository()->findAll();
    }

    public function getPaginator($sort, $filter)
    {
        return new Paginator($this->getRepository()->getFilterQuery($sort, $filter));
    }

    /**
     * @param string $file
     * @throws \Box\Spout\Common\Exception\IOException
     * @throws \Box\Spout\Reader\Exception\ReaderNotOpenedException
     */
    public function import(string $file)
    {
        $CITY_COL = 1;
        $REFERAL_COL = 0;

        $reader = ReaderEntityFactory::createXLSXReader();
        $reader->open($file);

        $cashedCity = [];
        $cashedNPA = [];
        $npa = $this->getAllItems();

        /** @var Npa $item */
        foreach ($npa as $item) {
            $cashedNPA[] = $item->getReferal();
        }

        $count = 0;

        foreach ($reader->getSheetIterator() as $sheet) {
            /** @var Row $row */
            foreach ($sheet->getRowIterator() as $key => $row) {
                if ($key === 1) continue;

                // do stuff with the row
                $cells = $row->getCells();

                if (count($cells) < 2) continue;

                $city = mb_strtolower($cells[$CITY_COL]->getValue());
                $referal = $cells[$REFERAL_COL]->getValue();

                if (empty($city) || empty($referal)) continue;

                if (!isset($cashedCity[$city])) {
                    $cityItem = $this->referenceService->getCityByName($city);

                    if ($cityItem) {
                        $cashedCity[$city] = $cityItem;
                    } else {
                        continue;
                    }
                } else {
                    $cityItem = $cashedCity[$city];
                }

                if (!in_array($referal, $cashedNPA)) {
                    $npa = new Npa();
                    $npa->setReferal($referal);
                    $npa->setCity($cityItem);
                    $npa->setUsesCount(0);
                    $npa->setName($referal);

                    $this->save($npa, false);
                    $cashedNPA[] = $referal;
                    $count++;
                }

                if ($count >200) {
                    $this->em->flush();
                    $count = 0;
                }
            }

            $this->em->flush();
            break;
        }

        $reader->close();
    }

    /**
     * @return NpaRepository
     */
    public function getRepository()
    {
        return $this->em->getRepository(Npa::class);
    }
}
